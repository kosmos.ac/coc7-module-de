# coc7-module-de

## Requirements

- Foundry Virtual Tabletop v11 or higher required
- `Call of Cthulhu 7th edition` system needs to be installed (`coc7`)

## Installation

1. Log-In to your Foundry Virtual Tabletop admin interface (https://your-foundry-vtt.url/setup)
2. in the top navigation go to `Add-On Modules` and click on `Install Module`
3. at the very bottom of the new window paste the following link into `Manifest URL` field, click on `Install`. 
> https://gitlab.com/kosmos.ac/coc7-module-de/-/raw/master/module.json
4. Start your `coc7` world and enable the module in module settings
